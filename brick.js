document.addEventListener("DOMContentLoaded", () => {
	console.log("DOM fully loaded and parsed");
	//window.addEventListener("keydown", onGame);
	//window.addEventListener("keydown", offGame);
	window.addEventListener("keydown", GO);

	// LES CONSTANTES
	//
	// VIESSE DU JEU
	const TIMESET = 10;
	let ballSpeed_X = 5;

	let ballSpeed_Y = 5;
	// LA BALLE DEPART
	let BigStart = 0;
	let startBall;
	console.log("🚀 ~ document.addEventListener ~ BigStart:", BigStart);
	let ballDiamPX;
	let ballDiam;
	let ballRayon;
	//
	let ball_CenterX;
	let ball_CenterY;
	//
	const brikW = 100;
	const brikH = 30;
	//
	let ball_X_HG;
	let ball_X_HD;
	let ball_X_BG;
	let ball_X_BD;
	//
	let ball_Y_HG;
	let ball_Y_HD;
	let ball_Y_BG;
	let ball_Y_BD;
	//
	// VARIABLES DE SIMPLIFICATIONS
	let ball_X_LEFT = ball_X_BG;
	let ball_X_RIGHT = ball_X_BD;
	let ball_Y_TOP = ball_Y_HG;
	let ball_Y_BOTTOM = ball_Y_BD;
	//
	//let ball_Poz;
	//
	let allBriks = [];

	//
	let iReady = document.getElementById("ready");

	console.log("🚀 ~ document.addEventListener ~ iReady:", iReady);
	let iWait = document.getElementById("wait");
	console.log("🚀 ~ document.addEventListener ~ iWait:", iWait);

	const iQuit = document.getElementById("quit");
	console.log("🚀 ~ document.addEventListener ~ iQuit:", iQuit);
	//const iReload = document.getElementById("reload");
	//console.log("🚀 ~ document.addEventListener ~ iReload:", iReload);

	//
	//const oneBrik = document.querySelector(".brik");
	const myFrame = document.querySelector(".frame");

	// USER
	const user = document.createElement("div");
	// Generates a random integer between 0 and 890 to be used as the user's position.
	const userPoz_X = Math.floor(Math.random() * 890);
	const userPoz_Y = 570 + "px";
	console.log("🚀 ~ ~ ~ ~ userPoz ~ ~ ~ ~:", userPoz_X);

	//
	// CREATION DE LA BALLE EN MEMOIRE
	const ball = document.createElement("div");
	ball.classList.add("ball");

	// STYLES PAR DEFAUT
	//LES STYLES PAR DEFAUT

	//iReady.style.display = "block";
	//iWait.style.display = "none";

	// LACEMENT DES FONCTIONS DE BASE
	ballRandom();
	placeBall();

	putUser();

	// LES FONCTIONS

	function oNoFF(element, toTogg) {
		console.log("🚀 ~ I C I TOGGLE AGNOSTIK");

		console.log("🚀 ~ oNoFF ~ toTogg:", toTogg);

		element.style.display = toTogg;
		console.log(
			element,
			"🚀 ~ oNoFF ~ element & style.display:",
			element.style.display
		);
	}
	//const iMready = document.getElementById("ready");

	//console.log("🚀 ~ document.addEventListener ~ iMready:", iMready);

	function GO(e) {
		e.preventDefault();
		//oNoFF(iReady, "display");
		//oNoFF(iWait, "none");

		if (e.key === " " && BigStart === 0) {
			BigStart = 1;
			console.dir(
				"1ME CONDITION",
				"BigStart : ",
				BigStart,
				"e.key : ",
				e.key
			);

			//onOff();

			//oNoFF(iReady, "noready");
			//oNoFF(iWait, "nowait");

			//oNoFF(iReady, "none");
			//oNoFF(iWait, "none");

			//console.log("🚀 ~ onGame ~ key:", e.key);

			console.log("🚀 ~ onGame ~ key:", e.key);
			window.addEventListener("keydown", moveKeyUser);
			//
			//

			moveBall();
			//
		} else if (e.key === "Escape") {
			BigStart = 0;
			console.dir(
				"2ME CONDITION",
				"BigStart : ",
				BigStart,
				"e.key : ",
				e.key
			);

			document.location.reload("localhost");

			//window.removeEventListener("keydown", moveKeyUser);
			//clearTimeout(startBall);
		} else if ((e.key === "p" || e.key === "P") && BigStart === 1) {
			//oNoFF(iWait, "display");
			//oNoFF(iReady, "none");
			clearTimeout(startBall);
			BigStart = 0;

			console.log("🚀 ~ P A U S E  ~ ", e.key);

			//window.removeEventListener("keydown", moveKeyUser);
		} else if ((e.key === "p" || e.key === "P") && BigStart === 0) {
			//oNoFF(iWait, "display");
			//oNoFF(iReady, "none");
			clearTimeout(startBall);
			BigStart = 0;

			console.log("🚀 ~ P A U S E  ~ ", e.key);

			//window.removeEventListener("keydown", moveKeyUser);
		}
	}

	function getbriks(x, y) {
		const brik = document.createElement("div");
		brik.classList.add("brik");
		brik.style.backgroundColor = randoColor();
		brik.setAttribute("id", x + y);
		myFrame.appendChild(brik);
		//let myW = Math.floor(Math.random() * 100) + "px";
		brik.style.left = x + "px";
		brik.style.top = y + "px";

		myFrame.appendChild(brik);
		//brik.style.left = Math.floor(Math.random() * 100) + "%";

		//
	}

	function detectBrik() {
		for (let x = 10; x < 900; x += 110) {
			console.log("🚀 ~  ~ x:", x);

			for (let y = 10; y < 240; y += 40) {
				console.log("🚀 ~  ~ y:", y);
				//randoColor();
				getbriks(x, y);
				allBriks.push([
					x,
					x + brikW,
					x,
					x + brikW,
					y,
					y,
					y + brikH,
					y + brikH,
				]);
			}
			console.log(
				"🚀 ~ document.addEventListener ~ allBriks:",
				allBriks
			);

			let allBrikEl = document.querySelectorAll(".brik");
			console.log(
				"🚀 ~ document.addEventListener ~ allBrikEl:",
				allBrikEl
			);
		}
	}

	class BrikUnik {
		constructor(xLine, yLine) {
			this.corner_L = [xLine, yLine + brikH];
			this.corner_R = [xLine + brikW];
			this.color = randoColor();
		}

		//
	}

	//
	function randoColor() {
		let rH = Math.round(Math.random() * 255 + 30);
		let vH = Math.round(Math.random() * 255 + 30);
		let bH = Math.round(Math.random() * 255 + 30);

		let oneRandoColor =
			"rgba(" + rH + "," + vH + "," + bH + "," + 0.5 + " )";

		//rgb(255 255 255 / .5)

		return oneRandoColor;
	}

	//let x = 10;

	function putUser() {
		console.log("🚀 ~ putUser ~ EN ACTION:");

		user.classList.add("user");
		user.style.left = userPoz_X + "px";
		user.style.top = userPoz_Y;
		myFrame.appendChild(user);

		//user.addEventListener("mousemove", moveMouseUser);
		//user.addEventListener("onk  ", clickUser);
	}

	function placeBall() {
		ball.style.left = ball_X_HG + "px";
		ball.style.top = ball_Y_HG + "px";
		myFrame.appendChild(ball);
		ballDiamPX = window.getComputedStyle(ball).width;
		ballDiam = parseFloat(ballDiamPX);
		ballRayon = ballDiam / 2;
		console.log("🚀 ~ placeBall ~ ballRayon:", ballRayon);
		console.log("🚀 ~ placeBall ~ ballDiamPX:", ballDiamPX);
	}

	//let ball_Poz = { ball_X_HG: 500, ball_Y_HG: 250 };
	//
	function ballRandom() {
		ball_X_HG = Math.round(Math.random() * 900 + 20);
		console.log("🚀 ~ ballRandom ~ ball_X_HG:", ball_X_HG);
		ball_Y_HG = Math.round(Math.random() * 250 + 250);
	}

	//
	//	function rePlaceUser() {
	//		user.style.left = userPoz_X + "px";
	//	}

	// MOVING THE BALL

	//let ballSpeedX = -5;
	//let ballSpeedY = ballSpeedX;

	//let ballX_Sens = 1;
	//let ballY_Sens = 1;
	//let startBall = setTimeout(moveBall);
	//let ball_X_LEFT = ball_X_BG;
	//let ball_X_RIGHT = ball_X_BD;
	//let ball_Y_TOP = ball_Y_HG;
	//let ball_Y_BOTTOM = ball_Y_BD;

	function colision() {
		if (ball_X_HG < 1 || ball_X_HD > 999) {
			ballSpeed_X = -ballSpeed_X;

			console.log(
				"🚀 ~ C O L I S I O N ~ - - - - - - - - -  Speed_X:",
				ballSpeed_X
			);
		}

		if (ball_Y_HG < 1 || ball_Y_BD > 599) {
			ballSpeed_Y = -ballSpeed_Y;

			console.log(
				"🚀 ~ C O L I S I O N Y  ~ - - - - - - - - - Speed_Y:",
				ballSpeed_Y
			);
		}

		//for (let i = 0; i < allBriks.length; i++) {
		//	console.log("🚀 ~~ allBriks[" + i + "][3]:", allBriks[i][0]);
		//	console.log("🚀 ~~ allBriks[" + i + "][2]:", allBriks[i][1]);
		//	console.log("🚀 ~~ allBriks[" + i + "][1]:", allBriks[i][2]);
		//	console.log("🚀 ~~ allBriks[" + i + "][0]:", allBriks[i][3]);
		//	// CONDITION GAME OVER
		//	if (ball_Y_BG >= 590) {
		//		let commit = " GAME OVER";
		//		console.log("🚀 ~ colision ~ commit:", commit);
		//	}
		//}

		//if (
		//	([ball_X_HG, ball_Y_HG] = allBriks.find(
		//		(element) => element[0] === ball_X_HG && element[1] === ball_Y_HG
		//	))
	}

	function gameOver() {}

	function moveBall() {
		ball_X_HG = parseFloat(ball.style.left);
		console.log("🚀 ~ moveBall ~ ball_X_HG:", ball_X_HG);
		ball_X_HD = ball_X_HG + ballDiam;
		console.log("🚀 ~ moveBall ~ ball_X_HD:", ball_X_HD);
		ball_X_BG = ball_X_HG;
		console.log("🚀 ~ moveBall ~ ball_X_BG:", ball_X_BG);
		ball_X_BD = ball_X_HD;
		console.log("🚀 ~ moveBall ~ ball_X_BD:", ball_X_BD);
		//
		ball_Y_HG = parseFloat(ball.style.top);
		console.log("🚀 ~ moveBall ~ ball_Y_HG:", ball_Y_HG);
		ball_Y_HD = ball_Y_HG;
		console.log("🚀 ~ moveBall ~ ball_Y_HD:", ball_Y_HD);
		ball_Y_BG = ball_Y_HG + ballDiam;
		console.log("🚀 ~ moveBall ~ ball_Y_BG:", ball_Y_BG);
		ball_Y_BD = ball_Y_BG;
		console.log("🚀 ~ moveBall ~ ball_Y_BD:", ball_Y_BD);

		//

		ball_CenterX = ball_X_HG + ballRayon;
		ball_CenterY = ball_Y_HG + ballRayon;

		console.log("🚀 CenterX:", ball_CenterX, "🚀 CenterY:", ball_CenterY);
		colision();

		ball.style.left = ball_X_HG + ballSpeed_X + "px";
		ball.style.top = ball_Y_HG + ballSpeed_Y + "px";
		startBall = setTimeout(moveBall, TIMESET);
	}

	// DEPLACEMENT DE LA RAQUETTE

	function moveKeyUser(e) {
		console.log("🚀 ~ moveKeyUser ~ e.key :", e.key);
		const speed = 25;
		//
		switch (e.key) {
			case "ArrowLeft":
				if (parseFloat(user.style.left) > 10) {
					console.log("🚀 ~ moveKeyUser ~ e.key:", e.key);
					//
					let actualPoz_X = parseFloat(user.style.left);
					console.log(
						"🚀 ~ moveKeyUser ~ actualPoz_X:",
						actualPoz_X
					);

					//

					user.style.left = actualPoz_X - speed + "px";
				}

				//rePlaceUser();

				break;

			//

			case "ArrowRight":
				if (parseFloat(user.style.left) < 899) {
					console.log("🚀 ~ moveKeyUser ~ e.key:", e.key);
					//
					let actualPoz_X = parseFloat(user.style.left);
					console.log(
						"🚀 ~ moveKeyUser ~ actualPoz_X:",
						actualPoz_X
					);

					//

					user.style.left = actualPoz_X + speed + "px";
				}

			//case "Escape":
			//	console.log("🚀 ~ GAME OVER ");
			//	clearTimeout(startBall);
			//	break;
		}

		//
	}

	// CREATE SUPER BRICK

	class SuperBrik {
		constructor(xAx, yAx) {
			this.corner_L = [xAx, yAx];
			this.corner_R = [xAx + brikW, yAx];
		}
	}

	// RECUPERATION HTML

	//let myGitlab = document.createElement("div");

	let myGitlab = "https://gitlab.com/eliazoura";

	//
	//
	//

	//

	//
});
